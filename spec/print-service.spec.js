import {PrintService} from '../src';
import {ConfigurationBase} from '@themost/common';
import fs from 'fs';
import path from 'path';
import util from 'util';
const writeFileAsync = util.promisify(fs.writeFile);

describe('PrintService', () => {
    it('should create instance', () => {
        const configuration = new ConfigurationBase();
        configuration.setSourceAt('settings/universis/reports', {
            "server": "https://example.com/jasperserver/"
        });
        const service = new PrintService({
            getConfiguration: function() {
                return configuration;
            }
        });
        expect(service).toBeTruthy();
    });
    it('should use PrintService.readReports()', async () => {
        const configuration = new ConfigurationBase();
        configuration.setSourceAt('settings/universis/reports', {
            "server": process.env.REPORT_SERVER,
            "user": process.env.REPORT_SERVER_USER,
            "password": process.env.REPORT_SERVER_PASSWORD
        });
        const service = new PrintService({
            getConfiguration: function() {
                return configuration;
            }
        });
        const reports = await service.readReports('reports/universis');
        expect(reports).toBeInstanceOf(Array);
    });
    it('should use PrintService.readReport()', async () => {
        const configuration = new ConfigurationBase();
        configuration.setSourceAt('settings/universis/reports', {
            "server": process.env.REPORT_SERVER,
            "user": process.env.REPORT_SERVER_USER,
            "password": process.env.REPORT_SERVER_PASSWORD
        });
        const service = new PrintService({
            getConfiguration: function() {
                return configuration;
            }
        });
        const reports = await service.readReports('reports/universis');
        // get first item
        const item = reports[0];
        expect(item).toBeTruthy('Expected a valid report unit');
        const reportUnit = await service.readReport(item.uri);
        expect(reportUnit).toBeTruthy();
    });
    it('should use PrintService.print()', async () => {
        const configuration = new ConfigurationBase();
        configuration.setSourceAt('settings/universis/reports', {
            "server": process.env.REPORT_SERVER,
            "user": process.env.REPORT_SERVER_USER,
            "password": process.env.REPORT_SERVER_PASSWORD
        });
        const service = new PrintService({
            getConfiguration: function() {
                return configuration;
            }
        });
        const buffer = await service.print('/reports/universis/Blank_A4', 'pdf', {
            REPORT_CLIENT_TOKEN: process.env.REPORT_CLIENT_TOKEN,
            ID: process.env.REPORT_ID
        });
        expect(buffer).toBeTruthy();
        await writeFileAsync(path.resolve(__dirname, '.tmp/Blank_A4.pdf'), buffer);
    });
});
