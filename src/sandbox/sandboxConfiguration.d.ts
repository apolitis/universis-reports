import {DataAdapterConfiguration} from '@themost/data';
export declare class SandboxConfiguration {
    public unattendedExecutionAccount: string;
    public adapter: DataAdapterConfiguration;
}
